﻿namespace Baemin.Commons.Constants
{
    public static class AppConstants
    {
        public static class EnvironmentName
        {
            public const string Development = "Development";
        }

        public static class ConfigurationLocation
        {
            public const string LocalMachine = "machine";
            //public const string S3 = "s3";
        }
    }
}

