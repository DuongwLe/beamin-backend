﻿namespace Baemin.Commons.Constants
{
    public static class OAuthConstants
    {
        public static class ExtendGrantType
        {
            public const string ApiRequest = "Baemin_api_request";
        }

        public static class ClaimTypes
        {
            public const string UserId = "userId";
            public const string UserType = "userType";
            public const string AccountId = "accountId";
        }

        public static class ErrorMessage
        {
            public const string UsernameOrPasswordEmpty = "UsernameOrPasswordEmpty";
            public const string CannotFindUser = "CannotFindUser";
            public const string PasswordIncorrect = "PasswordIncorrect";
        }
    }
}

