﻿namespace Baemin.Commons.Enums
{
    public enum DbContextFilterTypeEnum
    {
        None = 0,
        IsDeleted = 1,
        RestaurantId = 2,
        UserId = 4
    }
}

