﻿using System;
namespace Baemin.Commons.Enums
{
    public enum UserTypeEnum
    {
        SuperAdmin = 0,
        Admin = 1,
        RestaurantOwner = 2,
        Customer = 3
    }
}

