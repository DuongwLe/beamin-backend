﻿using System;
using System.Globalization;
using System.Text;
using Baemin.Commons.Enums;

namespace Baemin.Commons.Extentions
{
    public static class StringExtensions
    {
        public static string RemoveDiacritics(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static LanguageEnum ToLanguage(this string key)
        {
            return key switch
            {
                "en-US" => LanguageEnum.EN,
                "en" => LanguageEnum.EN,
                "vi-VN" => LanguageEnum.VN,
                "vi" => LanguageEnum.VN,
                _ => LanguageEnum.EN,
            };
        }
    }
}

