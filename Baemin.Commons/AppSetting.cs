﻿namespace Baemin.Commons
{
    public class AppSetting
    {
        public ApplicationSettings ApplicationSettings { get; set; }
        public ConnectionStrings ConnectionStrings { get; set; }
        public IdentityServer IdentityServer { get; set; }
        public SigningCredential SigningCredential { get; set; }
    }

    public class ApplicationSettings
    {
        public string ConfigurationFileLocation { get; set; }
        public string ConfigurationFilePath { get; set; }
    }

    public class ConnectionStrings
    {
        public string MainDbConnection { get; set; }
        public string IdentityDbConnection { get; set; }
    }

    public class IdentityServer
    {
        public string DefaultScheme { get; set; }
        public string Authority { get; set; }
        public string TokenEndpoint { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string ApiName { get; set; }
        public string ApiSecret { get; set; }
    }

    public class SigningCredential
    {
        public string CertificateFileLocation { get; set; }
        public string CertificateFilePath { get; set; }
        public string CertificateFilePassword { get; set; }
    }
}