﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class ProductStatus
    {
        public short ProductStatusId { get; set; }
        public string ProductStatusName { get; set; }
    }
}
