﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class UserInformation
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public virtual User User { get; set; }
    }
}
