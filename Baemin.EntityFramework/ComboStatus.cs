﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class ComboStatus
    {
        public short ComboStatusId { get; set; }
        public string ComboStatusName { get; set; }
    }
}
