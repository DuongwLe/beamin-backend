﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class CartComboMapping
    {
        public long CartId { get; set; }
        public long ComboId { get; set; }
        public int Quantity { get; set; }

        public virtual Cart Cart { get; set; }
        public virtual Combo Combo { get; set; }
    }
}
