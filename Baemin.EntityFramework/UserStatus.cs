﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class UserStatus
    {
        public short UserStatusId { get; set; }
        public string UserStatusName { get; set; }
    }
}
