﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class MainDbContext : DbContext
    {
        public MainDbContext()
        {
        }

        public MainDbContext(DbContextOptions<MainDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BankInformation> BankInformations { get; set; }
        public virtual DbSet<BusinessType> BusinessTypes { get; set; }
        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<CartComboMapping> CartComboMappings { get; set; }
        public virtual DbSet<CartProductMapping> CartProductMappings { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Combo> Combos { get; set; }
        public virtual DbSet<ComboMapping> ComboMappings { get; set; }
        public virtual DbSet<ComboStatus> ComboStatuses { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderStatus> OrderStatuses { get; set; }
        public virtual DbSet<PaymentMethodType> PaymentMethodTypes { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductStatus> ProductStatuses { get; set; }
        public virtual DbSet<Restaurant> Restaurants { get; set; }
        public virtual DbSet<RestaurantBusinessInformation> RestaurantBusinessInformations { get; set; }
        public virtual DbSet<RestaurantProductCategory> RestaurantProductCategories { get; set; }
        public virtual DbSet<RestaurantStatus> RestaurantStatuses { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserAddress> UserAddresses { get; set; }
        public virtual DbSet<UserConfirmationCode> UserConfirmationCodes { get; set; }
        public virtual DbSet<UserConfirmationType> UserConfirmationTypes { get; set; }
        public virtual DbSet<UserFavoriteRestaurant> UserFavoriteRestaurants { get; set; }
        public virtual DbSet<UserInformation> UserInformations { get; set; }
        public virtual DbSet<UserStatus> UserStatuses { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8mb4")
                .UseCollation("utf8mb4_0900_ai_ci");

            modelBuilder.Entity<BankInformation>(entity =>
            {
                entity.HasKey(e => e.BankId)
                    .HasName("PRIMARY");

                entity.ToTable("bank_information");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.BankCode)
                    .IsRequired()
                    .HasMaxLength(8)
                    .HasColumnName("bank_code");

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnName("bank_name");

                entity.Property(e => e.BankShortName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("bank_short_name");
            });

            modelBuilder.Entity<BusinessType>(entity =>
            {
                entity.ToTable("business_type");

                entity.Property(e => e.BusinessTypeId)
                    .ValueGeneratedNever()
                    .HasColumnName("business_type_id");

                entity.Property(e => e.BusinessTypeName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("business_type_name");
            });

            modelBuilder.Entity<Cart>(entity =>
            {
                entity.ToTable("carts");

                entity.HasIndex(e => e.RestaurantId, "restaurant_id");

                entity.HasIndex(e => e.UserId, "user_id");

                entity.Property(e => e.CartId).HasColumnName("cart_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.RestaurantId).HasColumnName("restaurant_id");

                entity.Property(e => e.TotalPrice)
                    .HasPrecision(18, 2)
                    .HasColumnName("total_price");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Restaurant)
                    .WithMany(p => p.Carts)
                    .HasForeignKey(d => d.RestaurantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("carts_ibfk_2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Carts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("carts_ibfk_1");
            });

            modelBuilder.Entity<CartComboMapping>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cart_combo_mappings");

                entity.HasIndex(e => e.CartId, "cart_id");

                entity.HasIndex(e => e.ComboId, "combo_id");

                entity.Property(e => e.CartId).HasColumnName("cart_id");

                entity.Property(e => e.ComboId).HasColumnName("combo_id");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.Cart)
                    .WithMany()
                    .HasForeignKey(d => d.CartId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_combo_mappings_ibfk_1");

                entity.HasOne(d => d.Combo)
                    .WithMany()
                    .HasForeignKey(d => d.ComboId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_combo_mappings_ibfk_2");
            });

            modelBuilder.Entity<CartProductMapping>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cart_product_mappings");

                entity.HasIndex(e => e.CartId, "cart_id");

                entity.HasIndex(e => e.ProductId, "product_id");

                entity.Property(e => e.CartId).HasColumnName("cart_id");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.HasOne(d => d.Cart)
                    .WithMany()
                    .HasForeignKey(d => d.CartId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_product_mappings_ibfk_1");

                entity.HasOne(d => d.Product)
                    .WithMany()
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_product_mappings_ibfk_2");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category");

                entity.Property(e => e.CategoryId)
                    .ValueGeneratedNever()
                    .HasColumnName("category_id");

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("category_name");
            });

            modelBuilder.Entity<Combo>(entity =>
            {
                entity.ToTable("combos");

                entity.HasIndex(e => e.RestaurantId, "restaurant_id");

                entity.Property(e => e.ComboId).HasColumnName("combo_id");

                entity.Property(e => e.ComboName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("combo_name");

                entity.Property(e => e.ComboStatusId).HasColumnName("combo_status_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.ImageUrl)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("image_url");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled).HasColumnName("is_enabled");

                entity.Property(e => e.Price)
                    .HasPrecision(18, 2)
                    .HasColumnName("price");

                entity.Property(e => e.RestaurantId).HasColumnName("restaurant_id");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.Restaurant)
                    .WithMany(p => p.Combos)
                    .HasForeignKey(d => d.RestaurantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("combos_ibfk_1");
            });

            modelBuilder.Entity<ComboMapping>(entity =>
            {
                entity.HasKey(e => new { e.ComboId, e.ProductId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("combo_mappings");

                entity.HasIndex(e => e.ProductId, "product_id");

                entity.Property(e => e.ComboId).HasColumnName("combo_id");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.Combo)
                    .WithMany(p => p.ComboMappings)
                    .HasForeignKey(d => d.ComboId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("combo_mappings_ibfk_1");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ComboMappings)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("combo_mappings_ibfk_2");
            });

            modelBuilder.Entity<ComboStatus>(entity =>
            {
                entity.ToTable("combo_status");

                entity.Property(e => e.ComboStatusId)
                    .ValueGeneratedNever()
                    .HasColumnName("combo_status_id");

                entity.Property(e => e.ComboStatusName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("combo_status_name");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("orders");

                entity.HasIndex(e => e.UserAddressId, "user_address_id");

                entity.HasIndex(e => e.UserId, "user_id");

                entity.Property(e => e.OrderId).HasColumnName("order_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.DeliveryCost)
                    .HasPrecision(18, 2)
                    .HasColumnName("delivery_cost");

                entity.Property(e => e.OrderCode)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("order_code");

                entity.Property(e => e.OrderSequence).HasColumnName("order_sequence");

                entity.Property(e => e.OrderStatusId).HasColumnName("order_status_id");

                entity.Property(e => e.PaymentMethodTypeId).HasColumnName("payment_method_type_id");

                entity.Property(e => e.TotalPrice)
                    .HasPrecision(18, 2)
                    .HasColumnName("total_price");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserAddressId).HasColumnName("user_address_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.UserAddress)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.UserAddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orders_ibfk_2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orders_ibfk_1");
            });

            modelBuilder.Entity<OrderStatus>(entity =>
            {
                entity.ToTable("order_status");

                entity.Property(e => e.OrderStatusId)
                    .ValueGeneratedNever()
                    .HasColumnName("order_status_id");

                entity.Property(e => e.OrderStatusName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("order_status_name");
            });

            modelBuilder.Entity<PaymentMethodType>(entity =>
            {
                entity.ToTable("payment_method_type");

                entity.Property(e => e.PaymentMethodTypeId)
                    .ValueGeneratedNever()
                    .HasColumnName("payment_method_type_id");

                entity.Property(e => e.PaymentMethodTypeName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("payment_method_type_name");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.HasIndex(e => e.RestaurantId, "restaurant_id");

                entity.HasIndex(e => e.RestaurantProductCategoryId, "restaurant_product_category_id");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.ImageUrl)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("image_url");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled).HasColumnName("is_enabled");

                entity.Property(e => e.IsTopping).HasColumnName("is_topping");

                entity.Property(e => e.Price)
                    .HasPrecision(18, 2)
                    .HasColumnName("price");

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("product_name");

                entity.Property(e => e.ProductStatusId).HasColumnName("product_status_id");

                entity.Property(e => e.RestaurantId).HasColumnName("restaurant_id");

                entity.Property(e => e.RestaurantProductCategoryId).HasColumnName("restaurant_product_category_id");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.Restaurant)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.RestaurantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("products_ibfk_1");

                entity.HasOne(d => d.RestaurantProductCategory)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.RestaurantProductCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("products_ibfk_2");
            });

            modelBuilder.Entity<ProductStatus>(entity =>
            {
                entity.ToTable("product_status");

                entity.Property(e => e.ProductStatusId)
                    .ValueGeneratedNever()
                    .HasColumnName("product_status_id");

                entity.Property(e => e.ProductStatusName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("product_status_name");
            });

            modelBuilder.Entity<Restaurant>(entity =>
            {
                entity.ToTable("restaurants");

                entity.HasIndex(e => e.OwnerId, "restaurants_ibfk_1_idx");

                entity.Property(e => e.RestaurantId).HasColumnName("restaurant_id");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnName("address");

                entity.Property(e => e.CategoryId).HasColumnName("category_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.ImageUrl)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("image_url");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled).HasColumnName("is_enabled");

                entity.Property(e => e.IsPartner).HasColumnName("is_partner");

                entity.Property(e => e.Latitude).HasColumnName("latitude");

                entity.Property(e => e.Longitude).HasColumnName("longitude");

                entity.Property(e => e.OpenFromUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("open_from_utc");

                entity.Property(e => e.OpenToUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("open_to_utc");

                entity.Property(e => e.OwnerId).HasColumnName("owner_id");

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("phone_number");

                entity.Property(e => e.RestaurantName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("restaurant_name");

                entity.Property(e => e.RestaurantStatusId).HasColumnName("restaurant_status_id");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Restaurants)
                    .HasForeignKey(d => d.OwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("restaurants_ibfk_1");
            });

            modelBuilder.Entity<RestaurantBusinessInformation>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("restaurant_business_informations");

                entity.HasIndex(e => e.RestaurantId, "restaurant_id");

                entity.Property(e => e.BackIdCardImageUrl)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("back_id_card_image_url");

                entity.Property(e => e.BankAccountNumber)
                    .HasMaxLength(128)
                    .HasColumnName("bank_account_number");

                entity.Property(e => e.BankBranchName)
                    .HasMaxLength(128)
                    .HasColumnName("bank_branch_name");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.BusinessTypeId).HasColumnName("business_type_id");

                entity.Property(e => e.FrontIdCardImageUrl)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("front_id_card_image_url");

                entity.Property(e => e.RegisterCertificationImageUrl)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("register_certification_image_url");

                entity.Property(e => e.RestaurantId).HasColumnName("restaurant_id");

                entity.HasOne(d => d.Restaurant)
                    .WithMany()
                    .HasForeignKey(d => d.RestaurantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("restaurant_business_informations_ibfk_1");
            });

            modelBuilder.Entity<RestaurantProductCategory>(entity =>
            {
                entity.ToTable("restaurant_product_categories");

                entity.Property(e => e.RestaurantProductCategoryId).HasColumnName("restaurant_product_category_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled).HasColumnName("is_enabled");

                entity.Property(e => e.RestaurantProductCategoryName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("restaurant_product_category_name");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");
            });

            modelBuilder.Entity<RestaurantStatus>(entity =>
            {
                entity.ToTable("restaurant_status");

                entity.Property(e => e.RestaurantStatusId)
                    .ValueGeneratedNever()
                    .HasColumnName("restaurant_status_id");

                entity.Property(e => e.RestaurantStatusName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("restaurant_status_name");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.PasswordHashed)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .HasColumnName("password_hashed");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .HasColumnName("salt");

                entity.Property(e => e.StatusId)
                    .HasColumnName("status_id")
                    .HasDefaultValueSql("'500'");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("user_name");

                entity.Property(e => e.UserTypeId).HasColumnName("user_type_id");
            });

            modelBuilder.Entity<UserAddress>(entity =>
            {
                entity.ToTable("user_addresses");

                entity.HasIndex(e => e.UserId, "user_id");

                entity.Property(e => e.UserAddressId).HasColumnName("user_address_id");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnName("address");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.Latitude).HasColumnName("latitude");

                entity.Property(e => e.Longitude).HasColumnName("longitude");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserAddressName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("user_address_name");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAddresses)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_addresses_ibfk_1");
            });

            modelBuilder.Entity<UserConfirmationCode>(entity =>
            {
                entity.HasKey(e => e.ConfirmationId)
                    .HasName("PRIMARY");

                entity.ToTable("user_confirmation_codes");

                entity.HasIndex(e => e.UserId, "user_id");

                entity.Property(e => e.ConfirmationId).HasColumnName("confirmation_id");

                entity.Property(e => e.ConfirmationCode)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("confirmation_code");

                entity.Property(e => e.ConfirmationTypeId).HasColumnName("confirmation_type_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.ExpiredTime)
                    .HasColumnType("datetime")
                    .HasColumnName("expired_time");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserConfirmationCodes)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_confirmation_codes_ibfk_1");
            });

            modelBuilder.Entity<UserConfirmationType>(entity =>
            {
                entity.HasKey(e => e.ConfirmationTypeId)
                    .HasName("PRIMARY");

                entity.ToTable("user_confirmation_type");

                entity.Property(e => e.ConfirmationTypeId)
                    .ValueGeneratedNever()
                    .HasColumnName("confirmation_type_id");

                entity.Property(e => e.ConfirmationTypeName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("confirmation_type_name");
            });

            modelBuilder.Entity<UserFavoriteRestaurant>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("user_favorite_restaurants");

                entity.HasIndex(e => e.RestaurantId, "restaurant_id");

                entity.HasIndex(e => e.UserId, "user_id");

                entity.Property(e => e.RestaurantId).HasColumnName("restaurant_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Restaurant)
                    .WithMany()
                    .HasForeignKey(d => d.RestaurantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_favorite_restaurants_ibfk_2");

                entity.HasOne(d => d.User)
                    .WithMany()
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_favorite_restaurants_ibfk_1");
            });

            modelBuilder.Entity<UserInformation>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("user_informations");

                entity.HasIndex(e => e.UserId, "user_id");

                entity.Property(e => e.Email)
                    .HasMaxLength(128)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(128)
                    .HasColumnName("first_name");

                entity.Property(e => e.LastName)
                    .HasMaxLength(128)
                    .HasColumnName("last_name");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(128)
                    .HasColumnName("phone_number");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany()
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_informations_ibfk_1");
            });

            modelBuilder.Entity<UserStatus>(entity =>
            {
                entity.ToTable("user_status");

                entity.Property(e => e.UserStatusId)
                    .ValueGeneratedNever()
                    .HasColumnName("user_status_id");

                entity.Property(e => e.UserStatusName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("user_status_name");
            });

            modelBuilder.Entity<UserType>(entity =>
            {
                entity.ToTable("user_type");

                entity.Property(e => e.UserTypeId)
                    .ValueGeneratedNever()
                    .HasColumnName("user_type_id");

                entity.Property(e => e.UserTypeName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("user_type_name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
