﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class BusinessType
    {
        public short BusinessTypeId { get; set; }
        public string BusinessTypeName { get; set; }
    }
}
