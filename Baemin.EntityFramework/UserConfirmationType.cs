﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class UserConfirmationType
    {
        public short ConfirmationTypeId { get; set; }
        public string ConfirmationTypeName { get; set; }
    }
}
