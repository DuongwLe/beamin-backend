﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class OrderStatus
    {
        public short OrderStatusId { get; set; }
        public string OrderStatusName { get; set; }
    }
}
