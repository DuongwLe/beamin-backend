﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class RestaurantStatus
    {
        public short RestaurantStatusId { get; set; }
        public string RestaurantStatusName { get; set; }
    }
}
