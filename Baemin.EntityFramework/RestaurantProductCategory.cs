﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class RestaurantProductCategory
    {
        public RestaurantProductCategory()
        {
            Products = new HashSet<Product>();
        }

        public long RestaurantProductCategoryId { get; set; }
        public string RestaurantProductCategoryName { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
