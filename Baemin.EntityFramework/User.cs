﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class User
    {
        public User()
        {
            Carts = new HashSet<Cart>();
            Orders = new HashSet<Order>();
            Restaurants = new HashSet<Restaurant>();
            UserAddresses = new HashSet<UserAddress>();
            UserConfirmationCodes = new HashSet<UserConfirmationCode>();
        }

        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string PasswordHashed { get; set; }
        public string Salt { get; set; }
        public short UserTypeId { get; set; }
        public int StatusId { get; set; }
        public bool? IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual ICollection<Cart> Carts { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Restaurant> Restaurants { get; set; }
        public virtual ICollection<UserAddress> UserAddresses { get; set; }
        public virtual ICollection<UserConfirmationCode> UserConfirmationCodes { get; set; }
    }
}
