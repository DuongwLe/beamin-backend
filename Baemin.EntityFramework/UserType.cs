﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class UserType
    {
        public short UserTypeId { get; set; }
        public string UserTypeName { get; set; }
    }
}
