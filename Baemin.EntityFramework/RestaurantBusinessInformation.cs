﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class RestaurantBusinessInformation
    {
        public long RestaurantId { get; set; }
        public string FrontIdCardImageUrl { get; set; }
        public string BackIdCardImageUrl { get; set; }
        public long? BankId { get; set; }
        public string BankBranchName { get; set; }
        public string BankAccountNumber { get; set; }
        public short BusinessTypeId { get; set; }
        public string RegisterCertificationImageUrl { get; set; }

        public virtual Restaurant Restaurant { get; set; }
    }
}
