﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class Product
    {
        public Product()
        {
            ComboMappings = new HashSet<ComboMapping>();
        }

        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public long RestaurantId { get; set; }
        public long RestaurantProductCategoryId { get; set; }
        public string ImageUrl { get; set; }
        public decimal Price { get; set; }
        public bool IsTopping { get; set; }
        public short ProductStatusId { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual Restaurant Restaurant { get; set; }
        public virtual RestaurantProductCategory RestaurantProductCategory { get; set; }
        public virtual ICollection<ComboMapping> ComboMappings { get; set; }
    }
}
