﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class Order
    {
        public long OrderId { get; set; }
        public string OrderCode { get; set; }
        public long OrderSequence { get; set; }
        public Guid UserId { get; set; }
        public long UserAddressId { get; set; }
        public decimal? DeliveryCost { get; set; }
        public decimal TotalPrice { get; set; }
        public short PaymentMethodTypeId { get; set; }
        public short OrderStatusId { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual User User { get; set; }
        public virtual UserAddress UserAddress { get; set; }
    }
}
