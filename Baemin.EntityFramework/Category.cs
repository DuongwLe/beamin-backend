﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class Category
    {
        public short CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
