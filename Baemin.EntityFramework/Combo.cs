﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class Combo
    {
        public Combo()
        {
            ComboMappings = new HashSet<ComboMapping>();
        }

        public long ComboId { get; set; }
        public string ComboName { get; set; }
        public long RestaurantId { get; set; }
        public string ImageUrl { get; set; }
        public decimal Price { get; set; }
        public short ComboStatusId { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual Restaurant Restaurant { get; set; }
        public virtual ICollection<ComboMapping> ComboMappings { get; set; }
    }
}
