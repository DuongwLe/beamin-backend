﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class ComboMapping
    {
        public long ComboId { get; set; }
        public long ProductId { get; set; }
        public int Quantity { get; set; }

        public virtual Combo Combo { get; set; }
        public virtual Product Product { get; set; }
    }
}
