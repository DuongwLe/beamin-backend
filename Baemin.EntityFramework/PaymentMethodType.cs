﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class PaymentMethodType
    {
        public short PaymentMethodTypeId { get; set; }
        public string PaymentMethodTypeName { get; set; }
    }
}
