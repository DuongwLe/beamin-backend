﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class BankInformation
    {
        public long BankId { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string BankShortName { get; set; }
    }
}
