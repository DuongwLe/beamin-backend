﻿using System;
using Baemin.Commons.Enums;
using Baemin.EntityFramework.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;

namespace Baemin.EntityFramework.Custom
{
    public class MainDbContextRestriction : MainDbContext, IDbContextFilterTypeCache
    {
        public int FilterType { get; private set; }
        public long RestaurantId { get; }
        public Guid UserId { get; }

        public MainDbContextRestriction(
           string connectionString,
           ILoggerFactory loggerFactory,
           params DbContextFilterTypeEnum[] filterTypes
           )
           : base(connectionString, loggerFactory)
        {
            RestaurantId = new long();
            UserId = Guid.Empty;

            FilterType = 0;
            if (filterTypes != null)
            {
                foreach (var filterType in filterTypes)
                {
                    FilterType |= (int)filterType;
                }
            }
        }

        public MainDbContextRestriction(
           string connectionString,
           ILoggerFactory loggerFactory,
           long restaurantId,
           params DbContextFilterTypeEnum[] filterTypes
           )
           : base(connectionString, loggerFactory)
        {
            RestaurantId = restaurantId;
            UserId = Guid.Empty;

            FilterType = 0;
            if (filterTypes != null)
            {
                foreach (var filterType in filterTypes)
                {
                    FilterType |= (int)filterType;
                }
            }
        }

        public MainDbContextRestriction(
           string connectionString,
           ILoggerFactory loggerFactory,
           Guid customerId,
           params DbContextFilterTypeEnum[] filterTypes
           )
           : base(connectionString, loggerFactory)
        {
            RestaurantId = new long();
            UserId = customerId;

            FilterType = 0;
            if (filterTypes != null)
            {
                foreach (var filterType in filterTypes)
                {
                    FilterType |= (int)filterType;
                }
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.ReplaceService<IModelCacheKeyFactory, DynamicModelCacheKeyFactory>();

        protected override void ApplyFilter(ModelBuilder modelBuilder)
        {
            FiltersExtensions.GlobalFilters(this, modelBuilder);
        }
    }
}

