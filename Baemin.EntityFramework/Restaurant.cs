﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Baemin.EntityFramework
{
    public partial class Restaurant
    {
        public Restaurant()
        {
            Carts = new HashSet<Cart>();
            Combos = new HashSet<Combo>();
            Products = new HashSet<Product>();
        }

        public long RestaurantId { get; set; }
        public Guid OwnerId { get; set; }
        public string RestaurantName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsPartner { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string ImageUrl { get; set; }
        public short CategoryId { get; set; }
        public short RestaurantStatusId { get; set; }
        public DateTime? OpenFromUtc { get; set; }
        public DateTime? OpenToUtc { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual User Owner { get; set; }
        public virtual ICollection<Cart> Carts { get; set; }
        public virtual ICollection<Combo> Combos { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
