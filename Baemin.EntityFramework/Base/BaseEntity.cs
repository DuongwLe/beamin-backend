﻿using System;
using Baemin.EntityFramework.Base;

namespace Baemin.EntityFramework.Base
{
    public abstract class DeleteEntity : IDeleteEntity
    {
        public bool IsDeleted { get; set; }
    }

    public abstract class UpsertEntity : IUpsertEntity
    {
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }
    }

    public abstract class UpsertAndDeleteEntity : IUpsertAndDeleteEntity
    {
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }
    }
}
