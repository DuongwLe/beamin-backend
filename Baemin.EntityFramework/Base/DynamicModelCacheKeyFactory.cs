﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Baemin.EntityFramework.Base
{
    public interface IDbContextFilterTypeCache
    {
        public int FilterType { get; }
        public long RestaurantId { get; }
        public Guid UserId { get; }
    }

    public class DynamicModelCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
        {
            if (context is IDbContextFilterTypeCache dynamicContext)
            {
                return (context.GetType(), dynamicContext.FilterType);
            }
            return context.GetType();
        }
    }
}

