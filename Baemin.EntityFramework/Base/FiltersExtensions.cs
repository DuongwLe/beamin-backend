﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Baemin.Commons.Enums;
using Microsoft.EntityFrameworkCore;

namespace Baemin.EntityFramework.Base
{
    public class FilterExpressionBuilder
    {
        private Type _type;
        private readonly List<Expression> _expressions;
        private readonly ParameterExpression _tableParam;

        public FilterExpressionBuilder(Type type)
        {
            _type = type;
            _tableParam = Expression.Parameter(type, "x");
            _expressions = new List<Expression>();
        }

        public FilterExpressionBuilder AddFilter(string field, Expression value)
        {
            var prop = Expression.Property(_tableParam, field);
            _expressions.Add(Expression.Equal(prop, Expression.Convert(value, prop.Type)));
            return this;
        }

        public FilterExpressionBuilder AddFilterListContains<T>(string field, Expression values)
        {
            var methodInfo = typeof(List<T>).GetMethod("Contains", new Type[] { typeof(T) });
            var prop = Expression.Property(_tableParam, field);
            _expressions.Add(Expression.Call(values, methodInfo, prop));
            return this;
        }

        public Expression GetFilter(string field, Expression value)
        {
            var prop = Expression.Property(_tableParam, field);
            return Expression.Equal(prop, value);
        }

        public Expression GetFilterListContains<T>(string field, Expression values)
        {
            var methodInfo = typeof(List<T>).GetMethod("Contains", new Type[] { typeof(T) });

            Expression prop = Expression.Convert(
                            Expression.Property(_tableParam, field),
                            typeof(T)
                        );

            return Expression.Call(values, methodInfo, prop);
        }

        public FilterExpressionBuilder AddFilterOr(Expression values1, Expression values2)
        {
            var orExpr = Expression.Or(values1, values2);
            _expressions.Add(orExpr);
            return this;
        }

        public LambdaExpression Build()
        {
            if (_expressions.Count > 0)
            {
                var ex = _expressions[0];
                for (var i = 1; i < _expressions.Count; i++)
                {
                    ex = Expression.AndAlso(ex, _expressions[i]);
                }
                var delegateType = typeof(Func<,>).MakeGenericType(_type, typeof(bool));

                return Expression.Lambda(delegateType, ex, _tableParam);
            }
            return null;
        }
    }

    public static class FiltersExtensions
    {
        public static void GlobalFilters(IDbContextFilterTypeCache ctx, ModelBuilder modelBuilder)
        {
            var ctxConstant = Expression.Constant(ctx);

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                var filterBuilder = new FilterExpressionBuilder(entityType.ClrType);

                var restaurantIdProp = entityType.FindProperty("RestaurantUd");
                if (restaurantIdProp != null && (ctx.FilterType & (int)DbContextFilterTypeEnum.RestaurantId) == (int)DbContextFilterTypeEnum.RestaurantId)
                {
                    var restaurantId = Expression.PropertyOrField(ctxConstant, "RestaurantUd");
                    filterBuilder.AddFilter("RestaurantUd", restaurantId);
                }

                var userIdProp = entityType.FindProperty("UserId");
                if (userIdProp != null && (ctx.FilterType & (int)DbContextFilterTypeEnum.UserId) == (int)DbContextFilterTypeEnum.UserId)
                {
                    var customerId = Expression.PropertyOrField(ctxConstant, "UserId");
                    filterBuilder.AddFilter("UserId", customerId);
                }

                var isDeletedProp = entityType.FindProperty("IsDeleted");
                if (isDeletedProp != null && (ctx.FilterType & (int)DbContextFilterTypeEnum.IsDeleted) == (int)DbContextFilterTypeEnum.IsDeleted)
                {
                    filterBuilder.AddFilter("IsDeleted", Expression.Constant(false));
                }

                entityType.SetQueryFilter(filterBuilder.Build());
            }
        }
    }
}

