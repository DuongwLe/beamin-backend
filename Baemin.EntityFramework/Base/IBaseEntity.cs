﻿using System;
namespace Baemin.EntityFramework.Base
{
    public interface IDeleteEntity
    {
        bool IsDeleted { get; set; }
    }

    public interface IUpsertEntity
    {
        DateTime CreatedAtUtc { get; set; }
        DateTime UpdatedAtUtc { get; set; }
        Guid? CreatedByUserId { get; set; }
        Guid? UpdatedByUserId { get; set; }
    }

    public interface IUpsertAndDeleteEntity : IDeleteEntity, IUpsertEntity
    {
    }
}

