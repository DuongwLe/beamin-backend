﻿using System;

namespace Baemin.Core
{
    public class BaeminException : Exception
    {
        public BaeminException()
        {
        }

        public BaeminException(string message) : base(message)
        {
        }

        public BaeminException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

