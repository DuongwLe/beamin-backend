﻿using System;
using Baemin.EntityFramework;

namespace Baemin.Core.Helpers
{
    public interface IDbContextHelper
    {
        MainDbContext GetCurrentMainDbContext(bool isIsolatedInstance = false);
    }
}

