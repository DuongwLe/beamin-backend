﻿using System;
using Baemin.Commons;
using Baemin.Commons.Enums;
using Baemin.EntityFramework;
using Baemin.EntityFramework.Custom;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Baemin.Core.Helpers.Implements
{
    public class DbContextHelper : IDbContextHelper
    {
        private readonly AppSetting _appSettings;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ICurrentContext _currentContext;

        private MainDbContext _currentMainDbContext;

        public DbContextHelper(
            IOptionsSnapshot<AppSetting> appSettings,
            ILoggerFactory loggerFactory,
            ICurrentContext currentContext
        )
        {
            _appSettings = appSettings.Value;
            _loggerFactory = loggerFactory;
            _currentContext = currentContext;
        }

        public MainDbContext GetCurrentMainDbContext(bool isIsolatedInstance = false)
        {
            if (!_currentContext.IsReady)
                throw new BaeminException("CurrentContext not ready!");

            var userTypeId = _currentContext.UserTypeId;
            var userId = _currentContext.UserId;
            var restaurantId = _currentContext.RestaurantId;

            if (isIsolatedInstance)
            {
                return CreateMainDbContext(userTypeId, userId, restaurantId);
            }

            if (_currentMainDbContext == null)
            {
                _currentMainDbContext = CreateMainDbContext(userTypeId, userId, restaurantId);
            }

            return _currentMainDbContext;
        }

        private MainDbContext CreateMainDbContext(UserTypeEnum? userTypeId, Guid? userId, long? restaurantId)
        {
            var connectionString = _appSettings.ConnectionStrings.MainDbConnection;

            if (userTypeId.HasValue && userTypeId == UserTypeEnum.Customer)
            {
                var filterTypes = new[] { DbContextFilterTypeEnum.UserId, DbContextFilterTypeEnum.IsDeleted };

                return new MainDbContextRestriction(connectionString, _loggerFactory, userId.Value, filterTypes);
            }

            if (userTypeId.HasValue && userTypeId == UserTypeEnum.RestaurantOwner)
            {
                var filterTypes = new[] { DbContextFilterTypeEnum.RestaurantId, DbContextFilterTypeEnum.IsDeleted };

                return new MainDbContextRestriction(connectionString, _loggerFactory, restaurantId.Value, filterTypes);
            }

            return new MainDbContext(connectionString, _loggerFactory);
        }
    }
}

