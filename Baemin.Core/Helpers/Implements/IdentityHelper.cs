﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Baemin.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Baemin.Core.Helpers.Implements
{
    public class IdentityHelper : IIdentityHelper
    {
        private readonly MainDbContext _dbContext;

        public IdentityHelper(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<long> GetRestaurantIdByRestaurantOwner(Guid userId, bool enableCache = false)
        {
            var restaurantId = await(
                from r in _dbContext.Restaurants
                where r.OwnerId == userId && r.IsEnabled && !r.IsDeleted
                select r.RestaurantId)
                .FirstOrDefaultAsync();
            return restaurantId;
        }
    }
}

