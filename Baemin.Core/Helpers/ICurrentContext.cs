﻿using System;
using Baemin.Commons.Enums;

namespace Baemin.Core.Helpers
{
    public interface ICurrentContext
    {
        bool IsReady { get; }

        Guid? UserId { get; }

        UserTypeEnum? UserTypeId { get; }

        long? RestaurantId { get; }
    }
}

