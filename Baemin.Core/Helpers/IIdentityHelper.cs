﻿using System;
using System.Threading.Tasks;

namespace Baemin.Core.Helpers
{
    public interface IIdentityHelper
    {
        Task<long> GetRestaurantIdByRestaurantOwner(Guid userId, bool enableCache = false);
    }
}

