﻿using Baemin.Main.API.Binders;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;

namespace Baemin.Main.API.Binders
{
    public class ApiInputBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context is null)
                throw new ArgumentNullException(nameof(context));

            if (context.Metadata.ModelType == typeof(string) && context.BindingInfo.BindingSource != BindingSource.Body)
                return new StringTrimmerBinder();

            if (context.Metadata.ModelType.IsClass && context.BindingInfo.BindingSource == BindingSource.Body)
                return Activator.CreateInstance(typeof(JsonInputBinder<>).MakeGenericType(context.Metadata.ModelType)) as IModelBinder;

            return null;
        }
    }
}