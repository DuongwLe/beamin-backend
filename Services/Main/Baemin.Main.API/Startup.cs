﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using Baemin.Commons;
using Baemin.Commons.Constants;
using Baemin.Commons.Enums;
using Baemin.Core.Helpers;
using Baemin.Core.Helpers.Implements;
using Baemin.EntityFramework;
using Baemin.Main.API.Binders;
using Baemin.Main.API.Filters;
using Baemin.Main.API.Middlewares;
using IdentityModel.AspNetCore.OAuth2Introspection;
using IdentityModel.Client;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Serilog;

namespace Baemin.Main.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AppSettings = new AppSetting();
            Configuration.Bind(AppSettings);
        }

        public IConfiguration Configuration { get; }
        public AppSetting AppSettings { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppSetting>(Configuration);

            services
                .Configure<RouteOptions>(options =>
                {
                    options.LowercaseUrls = true;
                    options.AppendTrailingSlash = true;
                })
                .Configure((Action<ApiBehaviorOptions>)(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                }));

            services
                .AddMvc(options =>
                {
                    options.EnableEndpointRouting = false;
                    options.MaxModelValidationErrors = 10;
                    options.Filters.Add<ValidateModelFilter>();
                    options.Filters.Add<CustomExceptionResponseFilter>();
                    options.ModelBinderProviders.Insert(0, new ApiInputBinderProvider());
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                });

            services
                .AddAuthentication(AppSettings.IdentityServer.DefaultScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = AppSettings.IdentityServer.Authority;
                    options.RequireHttpsMetadata = false;
                    options.ApiName = AppSettings.IdentityServer.ApiName;
                    options.ApiSecret = AppSettings.IdentityServer.ApiSecret;
                    options.EnableCaching = true;
                    options.CacheDuration = TimeSpan.FromMinutes(20);
                    options.TokenRetriever = new Func<HttpRequest, string>(req =>
                    {
                        var fromHeader = TokenRetrieval.FromAuthorizationHeader();
                        var fromQuery = TokenRetrieval.FromQueryString();
                        return fromHeader(req) ?? fromQuery(req);
                    });
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("AdminOnly", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim(OAuthConstants.ClaimTypes.UserType,
                        UserTypeEnum.Admin.ToString("d"),
                        UserTypeEnum.SuperAdmin.ToString("d"));
                });
                options.AddPolicy("CustomerOnly", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim(OAuthConstants.ClaimTypes.UserType, UserTypeEnum.Customer.ToString("d"));
                });
                options.AddPolicy("RestaurantOwnerOnly", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim(OAuthConstants.ClaimTypes.UserType, UserTypeEnum.RestaurantOwner.ToString("d"));
                });
            });
            services
                .Configure<TokenClientOptions>(options =>
                {
                    options.Address = AppSettings.IdentityServer.TokenEndpoint;
                    options.ClientId = AppSettings.IdentityServer.ClientId;
                    options.ClientSecret = AppSettings.IdentityServer.ClientSecret;
                })
                .AddTransient(sp => sp.GetRequiredService<IOptions<TokenClientOptions>>().Value)
                .AddHttpClient<TokenClient>();

            services
                .AddCors(options =>
                {
                    options.AddPolicy("allowAll", builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            ;
                    });
                })
                .AddResponseCompression();

            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Baemin.Main.API", Version = "v1" });
                    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Baemin.Main.API.xml"));
                    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Baemin.Main.ApiModel.xml"));
                    c.OperationFilter<Swashbuckle.AspNetCore.Filters.AppendAuthorizeToSummaryOperationFilter>();
                    c.OperationFilter<Swashbuckle.AspNetCore.Filters.SecurityRequirementsOperationFilter>();
                    c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                    {
                        Description = "Standard Authorization header using the Bearer scheme. Example: \"Bearer {your_access_token}\"",
                        In = ParameterLocation.Header,
                        Name = "Authorization",
                        Type = SecuritySchemeType.ApiKey,
                        Scheme = "Bearer"
                    });
                    c.DescribeAllParametersInCamelCase();
                });

            services
                .AddLogging()
                .AddOptions()
                .AddHttpContextAccessor();

            services
                .AddDbContext<MainDbContext>(options =>
                {
                    options
                        .UseMySql(
                            AppSettings.ConnectionStrings.MainDbConnection,
                            ServerVersion.AutoDetect(AppSettings.ConnectionStrings.MainDbConnection)
                        )
                        .EnableDetailedErrors();
                },
                ServiceLifetime.Scoped);

            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies().First(t => t.GetName().Name == "Baemin.Main.ApiAction"));

            // Singleton services

            // Scoped services
            services
                .AddScoped<ICurrentContext, CurrentHttpContext>()
                .AddScoped<IIdentityHelper, IdentityHelper>()
                .AddScoped<IDbContextHelper, DbContextHelper>();

            //services
            //   .AddHangfire(configuration => configuration
            //       .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
            //       .UseSimpleAssemblyNameTypeSerializer()
            //       .UseRecommendedSerializerSettings()
            //       .UseStorage(
            //           new MySqlStorage(
            //               AppSettings.ConnectionStrings.HangfireDbConnection,
            //               new MySqlStorageOptions
            //               {
            //                   TablesPrefix = "Hangfire"
            //               })
            //   ));
            //services.AddHangfireServer();

            //services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app
                .UseRouting()
                .UseCors("allowAll")
                .UseResponseCompression();

            app.UseAuthentication();
            app.UseAuthorization();

            app
                .UseSwagger()
                .UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Baemin.Main.API v1"));

            //app.UseEndpoints(endpoints =>
            //{
            //    //endpoints.MapControllers();
            //    endpoints.MapHub<NotificationHub>("/notifyhub");
            //});

            app.UseMiddleware<RequestInputLoggingMiddleware>();

            // app.UseHangfireDashboard();

            app.UseMvc();
        }
    }
}

