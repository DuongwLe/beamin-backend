﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using Baemin.Commons.Constants;
using Baemin.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Baemin.OAuth.Services
{
    public class BaeminAPIRequestGrantValidator : IExtensionGrantValidator
    {
        private readonly MainDbContext _dbContext;
        private readonly ITokenValidator _validator;

        public BaeminAPIRequestGrantValidator(ITokenValidator validator, MainDbContext dbContext)
        {
            _validator = validator;
            _dbContext = dbContext;
        }

        public string GrantType => OAuthConstants.ExtendGrantType.ApiRequest;

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            if (string.IsNullOrEmpty(context.Request.Raw.Get("userId")) || !Guid.TryParse(context.Request.Raw.Get("userId"), out var userId))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, OAuthConstants.ErrorMessage.CannotFindUser);
                return;
            }
            var user = await (from u in _dbContext.Users
                              where u.UserId == userId
                              select u).FirstOrDefaultAsync();
            if (user == null)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, OAuthConstants.ErrorMessage.CannotFindUser);
                return;
            }

            var customClaims = new List<Claim>()
            {
                new Claim(OAuthConstants.ClaimTypes.UserId, user.UserId.ToString()),
                new Claim(OAuthConstants.ClaimTypes.UserType, user.UserTypeId.ToString())
            };

            context.Result = new GrantValidationResult(
                subject: user.UserName,
                authenticationMethod: GrantType,
                claims: customClaims,
                identityProvider: "local",
                customResponse: new Dictionary<string, object>() { { "userId", user.UserId } }
            );
        }
    }
}