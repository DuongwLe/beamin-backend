﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using Baemin.Commons.Constants;
using Microsoft.EntityFrameworkCore;
using Sodium;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Baemin.EntityFramework;

namespace Baemin.OAuth.Services
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly MainDbContext _dbContext;

        public ResourceOwnerPasswordValidator(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            if (string.IsNullOrEmpty(context.UserName) || string.IsNullOrEmpty(context.Password))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, OAuthConstants.ErrorMessage.UsernameOrPasswordEmpty);
                return;
            }
            var user = await (from u in _dbContext.Users
                              where u.UserName == context.UserName
                              select u).FirstOrDefaultAsync();
            if (user == null)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, OAuthConstants.ErrorMessage.CannotFindUser);
                return;
            }
            if (!PasswordHash.ScryptHashStringVerify(user.PasswordHashed, context.Password + user.Salt))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.UnauthorizedClient, OAuthConstants.ErrorMessage.PasswordIncorrect);
                return;
            }

            var customClaims = new List<Claim>()
            {
                new Claim(OAuthConstants.ClaimTypes.UserId, user.UserId.ToString()),
                new Claim(OAuthConstants.ClaimTypes.UserType, user.UserTypeId.ToString())
            };

            context.Result = new GrantValidationResult(
                subject: user.UserName,
                authenticationMethod: "password",
                claims: customClaims,
                identityProvider: "local",
                customResponse: new Dictionary<string, object>() { { "userId", user.UserId } }
            );
        }
    }
}