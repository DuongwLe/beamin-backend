﻿using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Mvc;

namespace Baemin.OAuth.Controllers
{
    [Route("[controller]")]
    public class ApiResourceController : Controller
    {
        private readonly ConfigurationDbContext _configurationDbContext;

        public ApiResourceController(ConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }

        //[HttpPost]
        //[Route("")]
        //public async Task<IActionResult> CreateApiResource([FromBody] CreateApiResourceInputModel input)
        //{
        //    var apiResource = new ApiResource()
        //    {
        //        Name = input.Name,
        //        DisplayName = input.DisplayName,
        //        Description = input.Description,
        //        Enabled = true,
        //        ApiSecrets = { new Secret(input.Secret.Sha256()) },
        //        Scopes = input.Scopes
        //    };
        //    _configurationDbContext.ApiResources.Add(apiResource.ToEntity());
        //    return Ok(await _configurationDbContext.SaveChangesAsync() > 0);
        //}
    }
}

