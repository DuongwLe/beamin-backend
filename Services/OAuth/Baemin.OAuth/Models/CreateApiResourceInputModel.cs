﻿namespace Baemin.OAuth.Models
{
    public class CreateApiResourceInputModel
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Secret { get; set; }
        public string Description { get; set; }
        public string[] Scopes { get; set; }
    }
}