﻿using Microsoft.AspNetCore.Identity;

namespace Baemin.OAuth.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}