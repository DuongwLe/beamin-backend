﻿namespace Baemin.OAuth.Models
{
    public class CreateClientInputModel
    {
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientSecret { get; set; }
        public string[] Scopes { get; set; }
    }
}