﻿namespace Baemin.OAuth.Models
{
    public class ClientResponseModel
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string[] Scopes { get; set; }
        public string[] Grants { get; set; }
        public bool AllowOfflineAccess { get; set; }
    }
}